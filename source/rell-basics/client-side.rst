===========
Client Side
===========

This client tutorial is a continuation on the quickstart "city" example.
In this section we illustrate how to send transactions to and retrieve information from a blockchain node running Rell.

Try the example code
--------------------

First of all, we need to add a query to Rell source file:

::

    query is_city_registered(city_name: text): boolean {
          return (city @? { city_name }) != null;
    }

Clicking 'Start node' will start a Postchain node in a single-node mode which is convenient for testing.

The node builds blocks when there are transactions, or at least once every 30 seconds. It also has REST API we
can interact with to submit transactions and retrieve information.

The client code is written in JavaScript, this example uses the NodeJS environment. postchain-client-example_ can be downloaded using git:

::

    git clone https://bitbucket.org/chromawallet/postchain-client-example.git

To run it, execute:

.. code-block:: javascript

    npm install
    node index.js

This will create a transaction, sign it, submit to a node. And once transaction is added to a block, client will perform a query.

Now let's see how this client code can be implemented:

Install the client
------------------

We assume you have ``nodejs`` installed. The client library is called postchain-client_ and can be installed from npm.

Create an new directory for your test. Open a terminal in the new directory, initialize npm and install the client.

.. code-block:: javascript

    npm init -y
    npm install postchain-client --save

Connect to the node
-------------------

To connect to a Postchain node we need to know its REST API URL and blockchain identifier. DevPreview bundle comes with following defaults:

.. code-block:: javascript

    const pcl = require('postchain-client');

    // using default postchain node REST API port
    // On rellide-staging.chromia.dev, check node log for api url
    const node_api_url = "http://localhost:7740";

    // default blockchain identifier used for testing
    const blockchainRID = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";

    const rest = pcl.restClient.createRestClient((node_api_url, blockchainRID, 5);

Once we set up the information about the the REST Client connection, we can create the gtxClient connection.
This in particular, needs to receive the previous REST connection, the blockchainRID in Buffer format
and an array the names of the operations that you want call (at the moment this can be left empty):

.. code-block:: javascript

    const gtx = pcl.gtxClient.createClient(
        rest,
        Buffer.from(blockchainRID, 'hex'),
        []
    );

Now that the connection is set, you can start to create transactions and queries.

Make a transaction (with operations inside)
-------------------------------------------

You need to create the transaction client side, sign it with one or more keypairs, send it to the node and wait for it to be included into a block.

First, let's create the transaction and specify the public key of the person(s) that will sign it.
To create a random user keypair on the go you can use ``makeKeyPair()`` function.

.. code-block:: javascript

    const user = pcl.util.makeKeyPair();
    const tx = gtx.newTransaction([user.pubKey]);

Once it is created is possible call as many operations as you want.

.. code-block:: javascript

    tx.addOperation('insert_city', "Tel Aviv");
    tx.addOperation('insert_city', "Stockholm");
    /* etc */

Now, all is left is to sign and post the transaction

.. code-block:: javascript

    tx.sign(user.privKey, user.pubKey);
    tx.postAndWaitConfirmation();

Note: ``tx.postAndWaitConfirmation()`` returns a promise, and thus can be ``await``-ed.

Query
-----

Queries also make use of ``gtx`` client.

``gtx.query`` accepts as first parameter the name of the query as specified in the module and
then an object with as parameter name the variable name as specified in the query module.

E.g:

.. code-block:: javascript

  function is_city_registered(city_name) {
      return gtx.query("is_city_registered", {city_name: city_name});
  }

will work with query specified in the Rell file:

::

  query is_city_registered(city_name: text): boolean {
      return (city @? { city_name }) != null;
  }

Note: ``gtx.query(queryName, queryObject)`` also returns a promise.

Examples and further exercises
------------------------------

For now we have covered the basics of working with Rell. In the next section, we have prepared some examples of how to implement other functionalities in Rell and Chromia.


.. _postchain-client-example: https://bitbucket.org/chromawallet/postchain-client-example/src/master/
.. _postchain-client: https://www.npmjs.com/package/postchain-client
