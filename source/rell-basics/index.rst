=========================
Rell Basics
=========================

In this chapter we discuss fundamental concepts of Rell language.

- The :doc:`Main Concepts <./main-concepts>` section guide you through the concepts needed to create node backend with Rell.
- While :doc:`Client Side <./main-concepts>` discuss how to work with such backend using a Javascript client.

.. toctree::
   :hidden:

   main-concepts
   client-side
