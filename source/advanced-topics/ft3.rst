==========
FT3 Module
==========

FT3 is the recommended standards to handle accounts and tokens in Chromia ecosystem. It allow dapps to make full use of Chromia Vault, including Single Sign-On (SSO), assets transfer and visibility on the Vault's dapp explorer.

FT3 consists of Rell module which contains blockchain logic, and client side lib which provides js API for interaction with the module.

.. toctree::
   :maxdepth: 1

   ft3/ft3-features
   ft3/ft3-project-setup
   ft3/ft3-javascript-library
   ft3/ft3-rell-ft3-integration
   ft3/ft3-single-sign-on
   ft3/ft3-updating-a-chain

.. ft3/ft3-chromia-chat
