====================
NFA Basic Structures
====================

.. contents:: Table of Contents

-----

Source repository
-----------------

NFA Module can be cloned from its repository:

.. code-block:: text

  git clone https://bitbucket.org/chromawallet/nfa.git
  git checkout rell-0.10.0

Within repository root you will find:

- The ``rell`` directory which contains blockchain module,
- And ``client`` directory which contains javascript client.

Overview
--------

The NFA module involves 4 main entities:

nfa
  The ``nfa`` entity is used to define one class of assets:

  - ``nfa-human``
  - ``nfa-shoe``
  - ``nfa-spaceship``, etc.

entitee
  The ``entitee`` entity is *one* instance of a ``nfa``:

  - "Cloud Strife" ``entite`` of type ``nfa-human``.
  - "Apollo 8" ``entitee`` of type ``nfa-spaceship``.

component
  The ``component`` entity is used to define the data structure of a nfa's ``property``:

  - ``component-color`` is a ``text`` in hex format e.g. "#00BFFF".
  - ``component-month`` is an ``integer`` ranging from 1 to 12.

property
  The ``property`` entity is the characteristic of a nfa.

  From ECS perspective, ``property`` is the name given to a ``component`` in an ``entitee``:

  - ``entitee`` that belong to ``nfa-human`` have ``property-hair-color`` of type ``component-color``.
  - ``entitee`` that belong to ``nfa-car`` have ``property-warranty-period`` of type ``component-month``.

.. note::
  The ``property`` entity only *define* which property entitees of an nfa will have.

  Actual values are stored in other entities that are not mentioned in this document (we will access these values via utility functions).

The nfa entity
--------------

.. code-block:: rell

  entity nfa {
    name: text;
    key id: byte_array,
    desc: text;
    index max_entities: integer;
  }

name
  Is the name of the nfa. ``name`` should be unique within a single blockchain, but different chains can define different nfa with the same name.

id
  Is used as a unique key, formed by the hash of the ``name`` and the ``blockchain_rid``:

  ``(name, chain_context.blockchain_rid).hash()``

desc
  Is just the textual description of the nfa.

max_entities
  Is the maximum number of asset that can be created for this nfa. Minimum amount is 1 and max is ``integer.MAX_VALUE``.

The component entity
--------------------

.. code-block:: rell

  entity component {
      key id: byte_array;
      name: text;
      type: supported_types.type;
      extra: byte_array;
  }

id
  Same as in ``nfa``, the ``id`` is hash of ``name`` and ``blockchain_rid``

name
  Name of the structure ("color", "month", etc)

type
  Must be one of the following supported structure types:

  .. code-block:: rell

    enum type {
      TEXT,
      INTEGER,
      DECIMAL,
      BYTE_ARRAY,
      ENUM,
      NFA
    }

  - The first 4 are basic :doc:`rell types </languageguide/types>`.
  - ``enum`` is a mimic of enum type although internally it stores the value as text (and of course only accepts a subset of values).
  - Type ``nfa`` is a reference to another ``nfa`` item, this is particularly useful to introduce the concept of composition. eg ``nfa-human`` can own ``nfa-shoe`` or even a ``nfa-spaceship``, who knows?

extra
  For each of the above types there is an extra ``struct`` used to further describe accepted values.

  The ``extra`` attribute is saved as ``byte_array``, and can be converted back into ``struct`` with ``<struct_name>.from_bytes(extraByteArray)``

  (e.g. ``extra_decimal_component_structure.from_bytes(extraByteArray)`` for ``type.DECIMAL``)

  .. code-block:: rell

    struct extra_decimal_component_structure {
      min_val: integer;  // minimum value allowed (e.g. negative numbers not allowed)
      max_val: integer;  // max value allowed or byte_length
      decimals: integer; // how many decimals?
    }

  .. code-block:: rell

    struct extra_nfa_component_structure {
      nfa_id: byte_array; // nfa id
    }

  .. code-block:: rell

    struct extra_enum_component_structure {
      // this class is only used with enum types, assume enum are all text
      enums: list<text>;
    }

  .. note::

    For extra ``struct`` of other types, please check the corresponding file in ``nfa/core/supported_types``.

The property entity
-------------------

.. code-block:: rell

  entity property {
    nfa: nfa;
    component;
    name;
    is_required: boolean;
    is_mutable: boolean;

    key name, nfa;
  }

nfa
  Link to the nfa. ``Entitee``s of specified ``nfa`` will have this ``property``.

component
  Link to the component. This ``property`` value will have data structure respecting the rule of linked component.

name
  Name of the property.

is_required
  Is this component compulsory in the entitee?

is_mutable
  Can this value be changed once set?

The entitee entity
------------------

And finally the ``entitee`` is the nfa asset representation. So if nfa defines the family one asset belongs to, the entitee is the asset itself.

Entitees have one index mainly for fashion reason (e.g. claiming to have the first ever entity of a specific nfa).

You can mine entitees over time, or create them all at the same time. But there can only ever be maximum ``nfa.max_entities`` entitees of one nfa.

.. code-block:: rell

  entity entitee {
    idx: integer;
    key id: byte_array;
    nfa: nfa;

    key idx, nfa;
  }

idx
  index of creation (incremental starting from 0).

id
  id is passed by the creator of the entity. It's fine as long as it is unique. It's made for easy retrieval of a specific entitee.

nfa
  The ``nfa`` this entitee belongs to.

------

Next step
---------

Above we have described the main structures of NFA Module, in next sections we will explain how to work with nfas.

As mentioned previously, NFA can be manipulated on both blockchain side and client side. Depending on specific needs Dapps can choose either or both methods:

- :doc:`NFA on blockchain <./nfa-blockchain-side>`
- :doc:`NFA on client side <./nfa-client-side>`
