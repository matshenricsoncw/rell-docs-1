================
Client Side
================

.. contents:: Table of Contents

-----

Integrating NFA Module
----------------------

For project setup, please refer to :doc:`Project setup for FT3</advanced-topics/ft3/ft3-project-setup>`.

From the cloned NFA repository, copy ``client`` directory into your client's ``lib`` directory.

NFA JS client library provides seamless integration with NFA Rell module. Instead of calling low level blockchain operations (e.g. ``register_nfa``, ``update_property``, ``create_entity``, ...), it allows working with NFA assets as if they are plain JavaScript objects.

In the document, NFA class or simply NFA are interchangeably used to represent a type of an asset (the ``nfa`` entity), while NFA asset or entity are used to represent an instance of an NFA (the ``entitee`` entity).

Furthermore, looking from the perspective of Javascript, the ``component`` entity is usually refered as "property type" for ease-of-understanding.

NFA class
---------

NFA class can either be registered on client side, or loaded from blockchain.

Once loaded (or registered), it is a Javascript class that can be instantiated or extended as we see fit.

Registering new NFA
~~~~~~~~~~~~~~~~~~~

The simplest NFA without any properties can be defined like:

.. code-block:: js

  const Planet = nfa.class('Planet', {});

and registered on the blockchain:

.. code-block:: js

  await Planet.register(100, blockchain, user, accountId)

.. note::

  This function defines *new* NFA class for the dapp.

In most cases, NFA class will be defined on the blockchain side (we will soon add options to prevent exposing NFA definition to client side altogether).

In those case we need to load NFA classes from the blockchain:

Loading NFA from blockchain
~~~~~~~~~~~~~~~~~~~~~~~~~~~

NFA classes can be loaded by name:

.. code-block:: js

  const Planet = await nfa.loadClass('Planet', blockchain);

or by id which is equal to hash of nfa name and blockchain id:

.. code-block:: js

  const classId = gtv.gtvHash(['planet', blockchainId]);
  const Planet = await nfa.loadClassById(classId, blockchain);

NFA property and component
--------------------------

NFA can have one or more properties. A property can be one of the following types:

- integer
- decimal
- text
- enum
- byte array
- class (NFA)

.. note::

  These types are the property's ``component`` on blockchain side.

Property types come with resitrictions. When value is assigned to a property, validation will be performed against these restrictions and throw error if these requirements are not met:

.. code-block:: js

  const Planet = nfa.class('Planet', {
    x: nfa.integerDef({
      min: 0,
      max: 100
    })
  });

  const planet = new Planet({ x: 200 }); // throws an error because 200 is greater than max (100)

.. code-block:: js

  const Planet = nfa.class('Planet', {
    x: nfa.integerDef({
      required: true
    })
  });

  const planet = new Planet({}); // initializes planet with x == 0 (default value)

  planet.x = null; // throws an error because x is required

If typeof value and property differ, the lib will try to convert it to expected type. If conversion fails an error will be thrown.

.. code-block:: js

  const Color = nfa.enum('red', 'green', 'blue'); // define a new enum type

  const Car = nfa.class('Car', {
    color: Color
  });

  const car = new Car({ color: Color.red });

  car.color = Color.red; // assigns Color.red enum value to color property

  car.color = 'red';  // converts 'red' to Color.red and assigns it to color property

  car.color = 'gray'; // throws an error because 'gray' string cannot be converted to Color component

.. note::

  We will go into through the details of each supported type in the `Property types API`_ section below.

Creating and updating assets
----------------------------

The ``save`` function is used to create new asset or persist asset changes on the blockchain.

Depending on the state of an asset, ``save`` will automatically add operations from the following list to the transactions:

- ``create_entity`` - create new asset
- ``update_property`` - update property value
- ``delete_property`` - delete property (when property value is set to null)

For example, if asset is created on the client side it can be persisted on the blockchain by calling ``save``:

.. code-block:: js

  const planet = new Planet({ ... });

  await planet.save(blockchain, user, accountId);

Because this asset doesn’t exist on the blockchain yet, ``save`` function will call ``create_entity`` operation, and an account with id equal to ``accountId`` will be set as the owner of the asset.

.. note::

  Similar to ``register_nfa`` operation, ``create_entity`` operation is not protected and can be called by any user. Therefore it can be used for DOS attacks. So, in order to make a dapp more secure, it is important to protect the operation by adding rate limit (e.g. user is able call it only once), or completely disable it and have a dapp specific logic which creates assets and assigns them to accounts.

The ``update_property`` and ``delete_property`` operations are added by the save function when an asset is updated. ``save`` function checks every asset property if it is updated and then for each updated property adds corresponding ``update_property`` or ``delete_property`` operation to the transaction. If property is not updated, it will be ignored.

``update_property`` and ``delete_property`` operations are protected and can only be called by entity owner, otherwise the transaction will be rejected.


The Context class
-----------------

Calling ``save`` on an object should be enough if we are working with a single object or if objects are interconnected. Calling save on the top level object in the object hierarchy will save all the changes:

.. code-block:: js

  const Engine = nfa.class('Engine', {
    ...
  });

  const SpaceShip = nfa.class('SpaceShip', {
    engine: Engine
  });

  const ship = new SpaceShip({ engine: new Engine({ ... }) });

  await ship.save(blockchain, user, accountId);

In this example, ``save`` will persist both ``SpaceShip`` and ``Engine`` assets, because ``Engine`` is child of the ``SpaceShip`` asset.

If we have unrelated assets, then ``save`` has to be called on every updated asset:

.. code-block:: js

  const Dog = nfa.class('Dog', {
    name: nfa.text
  });

  const Cat = nfa.class('Cat', {
    name: nfa.text
  });

  const dog = new Dog({ name: 'Snoopy' });
  const cat = new Cat({ name: 'Garfield' });

  await dog.save(blockchain, user, accountId);
  await cat.save(blockchain, user, accountId);

Updates are executed independently for each entitee, i.e. dog will be updated in one transaction and cat in the other.

In some cases it is important to execute all updates in a single transaction, e.g. if one of the transactions fail for some reason the dapp will end up in an inconsistent state.

If multiple unrelated assets need to be created/updated in a single transaction then ``Context`` should be used instead.

``Context`` is initialized in the following way:

.. code-block:: js

  const context = new Context(
    blockchain, // FT3 Blockchain object
    user,       // FT3 User object for transaction signing
    ft3account  // FT3 Account object of the assets owner
  );

If we have two nfa classes ``Foo`` and ``Bar``, then we can use ``Context`` to save both entities in one transaction as follow:

.. code-block:: js

  const Bar = nfa.class('Bar', {
    x: nfa.integer
  });

  const Foo = nfa.class('Foo', {
    y: nfa.integer
  });

  const b = new Bar({ x: 10 });
  const f = new Foo({ y: 20 });

  context.add(b);
  context.add(f);
  await context.save();

Querying NFA entities
---------------------

Assets can be queried by their id or owner id, where owner id is FT3 account id.

There are two ways to perform query. The first is to call ``findById`` method on an NFA class:

.. code-block:: js

  const Car = await nfa.loadClass('Car', blockchain);
  const car = await Car.findById(carId, blockchain);

and the other way is using ``Context`` class. When using context class to query assets, type can be passed as the first argument:

.. code-block:: js

  const car = await context.findById(Car, carId);

or using only class name as string:

.. code-block:: js

  const car = await context.findById('Car', carId);

Passing asset type as first argument ensures that we are loading an asset of correct type, but usually we will only have asset id and don't know the type.

In this case ``null`` can be passed as first argument:

.. code-block:: js

  const someAsset = await context.findById(null, assetId);

From the response, context will check asset type and then load corresponding class from the blockchain (or from cache if class is already loaded), and instantiate it with asset data.

In our example game `Planetary <https://bitbucket.org/chromawallet/nfa_planetary/src/rell-0.10.1/>`_, ``null`` is passed in when querying for an asset which planet has found while exploring the space, because type of found asset is not known until it is loaded by id from the blockchain.


Property types API
------------------

In this section, we cover all supported types of nfa property and their options.

Integer
~~~~~~~

``integer`` is used to define integer properties on a nfa:

.. code-block:: js

  const Planet = nfa.class('Planet', {
      x: nfa.integer,
      y: nfa.integer,
  });

  await Planet.register(10, blockchain, user, accountId);

  const planet = new Planet({ x: 100, y: 200 });

Integer property can be customized, e.g. setting default value, making the property readonly, etc using ``nfa.integerDef`` function:

nfa.integerDef(options)
  ``readonly`` – defines if property is readonly

  ``default`` – value which property will have if property is not initialized in constructor.

  ``required`` – defines if property 	is required i.e. can be null

  ``min`` – sets property min value

  ``max`` – sets property max value

.. code-block:: js

  const Planet = nfa.class('Planet', {
    x: nfa.integerDef({
      default: 100
      readonly: true
    });
  });

  const planet = new Planet({});

  console.log(planet.x); // prints 100

  planet.x = 200; // throws an error because x is defined as readonly

  const planet2 = new Planet({ x: 200 }); // A value can be assigned to x only in constructor

nfa.integer
  Is equal to calling ``nfa.integerDef`` with default values:

.. code-block:: js

  // default values
  nfa.integerDef({
    readonly: false,
    required: false,
    default: 0,
    min: Number.MIN_SAFE_INTEGER,
    max: Number.MAX_SAFE_INTEGER
  });


Decimal
~~~~~~~

``decimal`` type is used to represent arbitrary precision decimal numbers. It internally uses decimal.js library.

.. code-block:: js

  const Planet = nfa.class('Planet', {
    x: nfa.decimal,
    y: nfa.decimal
  });

  const planet = new Planet({
    x: new Decimal(10.5),
    y: '5.0006',
  });

nfa.decimalDef(options)
  ``readonly`` – defines if property is readonly

  ``default`` – sets default property value, which is used if property is not initialized in constructor

  ``required`` – defines if property 	is required i.e. can be null

  ``min`` – sets property min value. Error is thrown when value less then min is assigned to property

  ``max`` – sets property max value. Error is thrown when value greater than is assigned to property

  ``decimals`` – sets decimal precision

nfa.decimal
  Is equal to calling ``nfa.decimalDef`` with default values:

.. code-block:: js

  // default values
  nfa.decimalDef({
    readonly: false,
    required: false,
    default: new Decimal(0.0),
    min: new Decimal(-Infinity),
    max: new Decimal(Infinity)
  });

Text
~~~~

``text`` is used to define string property on a nfa:

.. code-block:: js

  const Dog = nfa.class('Dog', {
    name: nfa.text
  });

  const dog = new Dog({ name: 'Snoopy' });

nfa.textDef(options)
  ``default`` – sets default property value, which is used if property is not initialized in constructor

  ``readonly`` – defines if property is readonly

  ``required`` – defines if property 	is required i.e. can be null

nfa.text
  Is equal to calling ``nfa.textDef`` with default values:

.. code-block:: js

  // default values
  nfa.textDef({
    readonly: false,
    required: false,
    default: null
  });

Enum
~~~~

``enum`` is used to define enum types.

.. code-block:: js

  const PetType = nfa.enum('dog', 'cat', 'parrot');

  const Pet = nfa.class('Pet', {
    type: PetType
    name: nfa.text
  });

  const cat = new Pet({
    type: PetType.cat,
    name: 'Garfield'
  });

nfa.enum(elements)
  ``elements`` - An array of enum values as string


Byte Array
~~~~~~~~~~

``byteArray`` type is used to store binary data. Internally it uses ``Buffer``.

.. code-block:: js

  const Car = nfa.class('Car', {
    image: nfa.byteArray
  });

  const car = new Car({ image: imageData });

Hexadecimal strings and Buffers can be assigned to a byteArray property:

.. code-block:: js

  car.image = '0123456789abcdef'; // valid assignment

  car.image = Buffer.from('some string'); // valid assignment

  car.image = 'some string'; // throws an error because string is not a hexadecimal value

nfa.byteArrayDef(options)
  ``default``

  ``readonly``

  ``required``

nfa.byteArray
  Is equal to calling ``nfa.byteArrayDef`` with default values:

.. code-block:: js

  nfa.byteArrayDef({
    readonly: false,
    required: false,
    default: null
  })


Class (NFA)
~~~~~~~~~~~

NFA can have other NFA as a child. This can be used to compose complex nfa structures.

.. code-block:: js

  const Engine = nfa.class('Engine', { ... });
  const Car = nfa.class('Car', {
    engine: Engine,
  });

NFA Class API
-------------

register(maxCount, blockchain, user)
  Register a new NFA class to the chain

  ``maxCount`` - maximum number of instances which can be created

  ``blockchain`` - FT3 Blockchain object

  ``user`` - FT3 User object

.. code-block:: js

  const Box = nfa.class('Box', {
    height: nfa.integer,
    width: nfa.integer,
  });

  await Box.register(100, blockchain, user);

findById(assetId, blockchain)
  Loads an asset by id.

  ``assetId`` - id of an asset

  ``blockchain`` - instance of a Blockchain class

.. code-block:: js

  const Planet = await nfa.loadClass('Planet', blockchain);
  const planet = await Planet.findById(planetId, blockchain);

findByOwner(accountId, blockchain)
  Returns an array of assets.

  ``accountId`` - id of a FT3 account which owns the asset

  ``blockchain`` - instance of a Blockchain class used to communicate with the blockchain

.. code-block:: js

  const Sword = await nfa.loadClass('Sword', blockchain);
  const swords = await Sword.findByOwner(accountId, blockchain);

NFA Asset API
-------------

save(blockchain, user, accountId)
  Updates or creates a new entity on a blockchain.

  ``blockchain`` - FT3 Blockchain object

  ``user`` - FT3 User object

  ``accountId`` - id of a FT3 account

.. code-block:: js

  const planet = await Planet.findById(planetId, blockchain);
  planet.x = 123;
  await planet.save(blockchain, user);

Context API
-----------

Context is responsible for keeping track of nfa entity updates and persisting them to the blockchain in a single transaction.


Context(blockchain, user, account)
  ``blockchain`` - FT3 Blockchain object

  ``user`` - FT3 User object for transaction signing

  ``account`` - FT3 Account object of the assets owner

.. code-block:: js

  import { util } from 'postchain-client';
  import { User, Blockchain, SingleSignatureAuthDescriptor, FlagType } from 'ft3-lib';
  import DirectoryService from './lib';

  const keyPair = util.makeKeyPair();
  const user = new User(
    keyPair,
    new SingleSignatureAuthDescriptor(
      keyPair.pubKey,
      [FlagType.Account, FlagType.Transfer]
    )
  )
  const blockchain = await Blockchain.initialize(
    Buffer.from('01234....', 'hex'),
    new DirectoryService()
  )
  const session = blockchain.newSession(user);
  const account = await session.findAccountById(accountId);
  const context = new Context(blockchain, user, account);

add(entity)
  Adds a nfa entity to context. Adding an entity to context ensures any changes made to the entity will persist the change to the blockchain on the next call to save method.

  ``entity`` - nfa entity

findById(class, id)
  ``class`` - name of the class as string, or the class itself (loaded with nfa.loadClass or created with nfa.class), or null (auto detection)

  ``id`` - the id of the entity

.. code-block:: js

  const planet = await context.findById('Planet', planetId);

.. code-block:: js

  const Planet = await nfa.loadClass('Planet', blockchain);
  const planet = await context.findById(Planet, planetId);

If ``null`` is passed as the first argument then context will query for any asset with id provided as second argument.

Returned entities are added to the context, so in case one of the entities is updated, on next call to context’s save method that change will be persisted on the blockchain.

save()
  Iterates through all the entities added to the context to find the properties which are updated, then builds a transaction with update operations for those properties. When transaction is built and signed it is sent to the blockchain node.

.. note::

  Only assets added to the context previously via ``context.findById`` will be updated.

.. code-block:: js

  const Planet = await nfa.loadClass('Planet', blockchain);
  const planet1 = await context.findById(Planet, planet1Id);
  const planet2 = await context.findById(Planet, planet2Id);

  planet1.x = 10;

  // update planet1
  await context.save();

  planet2.x = 20
  planet1.x = 20;

  update both planet1 and planet2
  await context.save();
