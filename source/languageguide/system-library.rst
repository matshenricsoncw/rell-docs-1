========================
System Library (Globals)
========================

.. contents:: Table of Contents

-----

Entities
--------

::

    entity block {
        block_height: integer;
        block_rid: byte_array;
        timestamp;
    }

    entity transaction {
        tx_rid: byte_array;
        tx_hash: byte_array;
        tx_data: byte_array;
        block;
    }

It is not possible to create, modify or delete values of those entities in code.

--------------

Namespaces
----------

chain_context
~~~~~~~~~~~~~

``chain_context.args: module_args`` - module arguments specified in ``raw_config`` under path ``gtx.rell.moduleArgs.<module name>``.
The type is ``module_args``, which must be a user-defined struct. If no ``module_args`` struct is defined in the module,
the ``args`` field cannot be accessed.

Example of ``module_args``:

::

    struct module_args {
        s: text;
        n: integer;
    }

Corresponding module configuration:

::

    {
        "gtx": {
            "rell": {
                "moduleArgs": {
                    "module_name": {
                        "s": "Hello",
                        "n": 123
                    }
                }
            }
        }
    }

Code that reads ``module_args``:

::

    function f() {
        print(chain_context.args.s);
        print(chain_context.args.n);
    }

Every module can have its own ``module_args``. Reading ``chain_context.args`` returns the args for the current module, and
the type of ``chain_context.args`` is different for different modules: it is the ``module_args`` struct defined in that module.

``chain_context.blockchain_rid: byte_array`` - blockchain RID

``chain_context.raw_config: gtv`` - blockchain configuration object, e. g. ``{"gtx":{"rell":{"mainFile":"main.rell"}}}``

op_context
~~~~~~~~~~

System namespace ``op_context`` can be used only in an operation or a function called from an operation, but not in a query.

``op_context.block_height: integer`` - the height of the block currently being built
(equivalent of ``op_context.transaction.block.block_height``).

``op_context.last_block_time: integer`` - the timestamp of the last block, in milliseconds
(like ``System.currentTimeMillis()`` in Java). Returns ``-1`` if there is no last block (the block currently being built
is the first block).

``op_context.transaction: transaction`` - the transaction currently being built.

--------------

Global Functions
----------------

| ``abs(integer): integer`` - absolute value
| ``abs(decimal): decimal``

| ``empty(T?): boolean`` - returns ``true`` if the argument is ``null`` or an empty collection and ``false`` otherwise;
  for nullable collections checks both conditions
| ``empty(list<T>): boolean``
| ``empty(set<T>): boolean``
| ``empty(map<K, V>): boolean``

| ``exists(T?): boolean`` - opposite to ``empty()``
| ``exists(list<T>): boolean``
| ``exists(set<T>): boolean``
| ``exists(map<K, V>): boolean``

``is_signer(byte_array): boolean`` - returns ``true`` if a byte array is
in the list of signers of current operation

``log(...)`` - print a message to the log (same usage as ``print``)

| ``max(integer, integer): integer`` - maximum of two values
| ``max(decimal, decimal): decimal``

| ``min(integer, integer): integer`` - minimum of two values
| ``min(decimal, decimal): decimal``

``print(...)`` - print a message to STDOUT:

-  ``print()`` - prints an empty line
-  ``print('Hello', 123)`` - prints ``"Hello 123"``

``verify_signature(message: byte_array, pubkey: pubkey, signature: byte_array): boolean`` - returns ``true``
if the given signature is a result of signing the message with a private key corresponding to the given public key

--------------

Require function
-----------------

For checking a boolean condition:

``require(boolean[, text])`` - throws an exception if the argument is ``false``

For checking for ``null``:

``require(T?[, text]): T`` - throws an exception if the argument is
``null``, otherwise returns the argument

``require_not_empty(T?[, text]): T`` - same as the previous one

For checking for an empty collection:

``require_not_empty(list<T>[, text]): list<T>`` - throws an exception if
the argument is an empty collection, otherwise returns the argument

``require_not_empty(set<T>[, text]): set<T>`` - same as the previous

``require_not_empty(map<K,V>[, text]): map<K,V>`` - same as the previous

When passing a nullable collection to ``require_not_empty``, it throws an
exception if the argument is either ``null`` or an empty collection.

Examples:

::

    val x: integer? = calculate();
    val y = require(x, "x is null"); // type of "y" is "integer", not "integer?"

    val p: list<integer> = get_list();
    require_not_empty(p, "List is empty");

    val q: list<integer>? = try_to_get_list();
    require(q);           // fails if q is null
    require_not_empty(q); // fails if q is null or an empty list
