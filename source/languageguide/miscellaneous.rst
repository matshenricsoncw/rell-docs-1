
Miscellaneous
=============

Comments
--------

Single-line comment:

::

    print("Hello"); // Some comment

Multiline comment:

::

    print("Hello"/*, "World"*/);
    /*
    print("Bye");
    */
