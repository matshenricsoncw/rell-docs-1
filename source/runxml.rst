==================
Run.XML
==================

Run.XML format is used to define a run-time configuration of a Rell node. The configuration consists of two key parts:

#. The list of Postchain nodes (the target node is one of those nodes).
#. The list of blockchains, each having an associated configuration(s) and a Rell application.

The format is used:

* By Rell command-line utilities ``multirun.sh`` and ``multigen.sh``.
* By the Eclipse IDE (which internally uses ``multirun.sh`` to launch Postchain applications).

The Format
=================

Example of a Run.XML file:

.. code-block:: xml

    <run wipe-db="true">
        <nodes>
            <config src="config/node-config.properties" add-signers="false" />
        </nodes>
        <chains>
            <chain name="user" iid="1">
                <config height="0">
                    <app module="user" />
                    <gtv path="gtx/rell/moduleArgs/user">
                        <dict>
                            <entry key="foo"><bytea>0373599a61cc6b3bc02a78c34313e1737ae9cfd56b9bb24360b437d469efdf3b15</bytea></entry>
                        </dict>
                    </gtv>
                </config>
                <config height="1000">
                    <app module="user_1000">
                        <args module="user_1000">
                            <arg key="foo"><bytea>0373599a61cc6b3bc02a78c34313e1737ae9cfd56b9bb24360b437d469efdf3b15</bytea></arg>
                        </args>
                    </app>
                    <gtv path="path" src="config/template.xml"/>
                </config>
            </chain>
            <chain name="city" iid="2">
                <config height="0" add-dependencies="false">
                    <app module="city" />
                    <gtv path="signers">
                        <array>
                            <bytea>0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57</bytea>
                        </array>
                    </gtv>
                    <dependencies>
                        <dependency name="user" chain="user" />
                    </dependencies>
                </config>
                <include src="config/city-include-1.xml"/>
                <include src="config/city-include-2.xml" root="false"/>
            </chain>
        </chains>
    </run>

Top-level elements are:

* ``nodes`` - defines Postchain nodes
* ``chains`` - defines blockchains

Nodes
~~~~~~~

Node configuration is provided in a standard Postchain ``node-config.properties`` format.

Specifying a path to an existing ``node-config.properties`` file (path is relative to the Run.XML file):

.. code-block:: xml

    <nodes>
        <config src="config/node-config.properties" add-signers="false" />
    </nodes>

Specifying node configuration properties directly, as text:

.. code-block:: xml

    <nodes>
        <config add-signers="false">
            database.driverclass=org.postgresql.Driver
            database.url=jdbc:postgresql://localhost/postchain
            database.username=postchain
            database.password=postchain
            database.schema=test_app

            activechainids=1

            api.port=7740
            api.basepath=

            node.0.id=node0
            node.0.host=127.0.0.1
            node.0.port=9870
            node.0.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57

            messaging.privkey=3132333435363738393031323334353637383930313233343536373839303131
            messaging.pubkey=0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57
        </config>
    </nodes>

Chains
~~~~~~~~~~~

A ``chain`` element can have multiple ``config`` elements and a ``dependencies`` element inside.

A single chain may have specific configurations assigned to specific block heights.

.. code-block:: xml

    <config height="0" add-dependencies="false">
        <app module="city" />
        <gtv path="signers">
            <array>
                <bytea>0350fe40766bc0ce8d08b3f5b810e49a8352fdd458606bd5fafe5acdcdc8ff3f57</bytea>
            </array>
        </gtv>
    </config>

An ``app`` element specifies a Rell application used by the chain. Attribute ``module`` is the name of the main module of the app.
The source code of the main module and all modules it imports will be injected into the generated blockchain XML configuration.

Elements ``gtv`` are used to inject GTXML fragments directly into the generated Postchain blockchain XML configuration.
Attribute ``path`` specifies a dictionary path for the fragment (default is root). For example, the fragment

.. code-block:: xml

    <gtv path="gtx/rell/moduleArgs/user">
        <dict>
            <entry key="foo"><bytea>0373599a61cc6b3bc02a78c34313e1737ae9cfd56b9bb24360b437d469efdf3b15</bytea></entry>
        </dict>
    </gtv>

will produce a blockchain XML:

.. code-block:: xml

    <dict>
        <entry key="gtx">
            <dict>
                <entry key="rell">
                    <dict>
                        <entry key="moduleArgs">
                            <dict>
                                <entry key="user">
                                    <dict>
                                        <entry key="foo">
                                            <bytea>0373599A61CC6B3BC02A78C34313E1737AE9CFD56B9BB24360B437D469EFDF3B15</bytea>
                                        </entry>
                                    </dict>
                                </entry>
                            </dict>
                        </entry>
                    </dict>
                </entry>
            </dict>
        </entry>
    </dict>

GTXML contents to be injected shall be either specified as a nested element of a ``gtv`` element, or placed in an XML file
referenced via the ``src`` attribute.

Included files
~~~~~~~~~~~~~~~

Other XML files can be included anywhere in a Run.XML using ``include`` tag. Included files may include other XML files
as well.

Including a file with its root element replacing the ``include`` element:

.. code-block:: xml

    <include src="config/city-include-1.xml"/>

Including a file without its root element, the ``include`` is replaced by the child elements of the root element
of the file:

.. code-block:: xml

    <include src="config/city-include-2.xml" root="false"/>

Utilities
=======================

Those utilities are a part of the `Rell language <https://bitbucket.org/chromawallet/rellr>`_.

multirun.sh
~~~~~~~~~~~~~~~~

Runs an application described by a Run.XML configuration.

.. code-block:: none

    Usage: RellRunConfigLaunch [-d=SOURCE_DIR] RUN_CONFIG
    Launch a run config
          RUN_CONFIG   Run config file
      -d, --source-dir=SOURCE_DIR
                       Rell source code directory (default: current directory)

multigen.sh
~~~~~~~~~~~~~~

Creates a Postchain blockchain XML configuration from a Run.XML configuration.

.. code-block:: none

    Usage: RellRunConfigGen [--dry-run] [-d=SOURCE_DIR] [-o=OUTPUT_DIR] RUN_CONFIG
    Generate blockchain config from a run config
          RUN_CONFIG   Run config file
          --dry-run    Do not create files
      -d, --source-dir=SOURCE_DIR
                       Rell source code directory (default: current directory)
      -o, --output-dir=OUTPUT_DIR
                       Output directory

Example of a generated directory tree:

.. code-block:: none

    out/
    ├── blockchains
    │   ├── 1
    │   │   ├── 0.gtv
    │   │   ├── 0.xml
    │   │   ├── 1000.gtv
    │   │   ├── 1000.xml
    │   │   └── brid.txt
    │   └── 2
    │       ├── 0.gtv
    │       ├── 0.xml
    │       ├── 1000.gtv
    │       ├── 1000.xml
    │       ├── 2000.gtv
    │       ├── 2000.xml
    │       ├── 3000.gtv
    │       ├── 3000.xml
    │       └── brid.txt
    ├── node-config.properties
    └── private.properties
